#ifndef POISSON_PROBLEMS_HH
#define POISSON_PROBLEMS_HH

#include <cassert>
#include <cmath>

#include "temporalprobleminterface.hh"

template <class FunctionSpace>
class TimeDependentCosinusProduct : public TemporalProblemInterface < FunctionSpace >
{
  typedef TemporalProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  // get time function from base class
  using BaseType :: time ;

  TimeDependentCosinusProduct( const Dune::Fem::TimeProviderBase &timeProvider )
    : BaseType( timeProvider )
  {}

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {

/*********************************************************/
/***                 NEW FOR LESSON 2                  ***/
/*********************************************************/
    phi = RangeType(0);
/*********************************************************/

  }

  //! the exact solution
  virtual void u(const DomainType& x,
                 RangeType& phi) const
  {

/*********************************************************/
/***                 NEW FOR LESSON 2                  ***/
/*********************************************************/
    // no exact solution - method needed for initial data
    phi = RangeType(1);
/*********************************************************/

  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {

/*********************************************************/
/***                 NEW FOR LESSON 2                  ***/
/*********************************************************/
    // no exact solution
    ret = JacobianRangeType(0);
/*********************************************************/

  }
};

#endif // #ifndef POISSON_HH
