/*********************************************************/
/***                 NEW FOR LESSON 2                  ***/
/*********************************************************/

#ifndef DEFORMATION_HH
#define DEFORMATION_HH

#include <dune/common/exceptions.hh>
#include <dune/grid/geometrygrid/coordfunction.hh>
#include <dune/fem/space/common/functionspace.hh>

// DeformationCoordFunction
// ------------------------

template< int dimWorld >
struct DeformationCoordFunction
{
  typedef Dune::Fem::FunctionSpace< double, double, dimWorld, dimWorld > FunctionSpaceType;

  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;

  explicit DeformationCoordFunction ( const double time = 0.0 )
  : time_( time )
  {}

  void evaluate ( const DomainType &x, RangeType &y ) const
  {
    //const double newtime = (time_ < 0.5 ? 0.0 : (time_ < 0.75 ? 4*(time_ - 0.5) : 1.0));
    const double newtime = std::min( time_, 1.0 );

    const double r1 = std::abs( x[ 0 ] );
    const double target = (1.0 - (r1*r1))*((r1*r1) + 0.05) + (r1*r1)*sqrt(1.0 - (r1*r1));

    const double r2 = std::sqrt( x[1]*x[1] + x[2]*x[2] );
    const double factor = std::exp( -2*newtime )*r2 + (1.0 - std::exp( -2*newtime ))*target;

    y[ 0 ] = 2 * x[ 0 ] + newtime*(x[ 0 ] > 0 ? 2.0 : -1.0 )*x[ 0 ];
    y[ 1 ] = factor * x[ 1 ] / (r2 + 0.000001);
    y[ 2 ] = factor * x[ 2 ] / (r2 + 0.000001);
  }

  void setTime ( const double time ) { time_ = time; }

private:
  double time_;
};

//! deformation depending on a discrete function
template <class DiscreteFunctionType>
class DeformationDiscreteFunction
: public Dune::DiscreteCoordFunction< double, 3, DeformationDiscreteFunction< DiscreteFunctionType > >
{
  typedef Dune::DiscreteCoordFunction< double, 3, DeformationDiscreteFunction< DiscreteFunctionType > > BaseType;

  typedef typename DiscreteFunctionType :: GridType GridType ;
  typedef typename DiscreteFunctionType :: LocalFunctionType LocalFunctionType ;
  typedef typename DiscreteFunctionType :: RangeType  RangeType ;
public:
  DeformationDiscreteFunction ( const DiscreteFunctionType& vertices )
  : vertices_( vertices )
  {}

  template< class HostEntity , class RangeVector >
  void evaluate ( const HostEntity &hostEntity, unsigned int corner,
                  RangeVector &y ) const
  {
    DUNE_THROW(Dune::NotImplemented,"evaluate not implemented for codim > 0");
  }

  template <class RangeVector>
  void evaluate ( const typename GridType :: template Codim<0>::Entity &entity,
		  unsigned int corner,
                  RangeVector &y ) const
  {
    y = entity.geometry()[corner];

    return;
    typedef typename GridType::ctype  ctype;
    enum { dim = GridType::dimension };

    const Dune::ReferenceElement< ctype, dim > &refElement
      = Dune::ReferenceElements< ctype, dim >::general( entity.type() );

    LocalFunctionType localVertices = vertices_.localFunction( entity );

    localVertices.evaluate( refElement.position( corner, dim ), y );
  }

  void setTime ( const double time )
  {
  }

protected:
  const DiscreteFunctionType& vertices_;
};

#endif // #ifndef DEFORMATION_HH

/*********************************************************/

