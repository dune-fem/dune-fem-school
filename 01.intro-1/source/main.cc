// always include this file
#include <config.h>

// C++ includes
#include <cmath>
#include <iostream>
#include <string>

// dune-common includes
#include <dune/common/exceptions.hh>                // adds Dune::Exception

// dune-fem includes
#include <dune/fem/gridpart/filteredgridpart.hh>    // adds Dune::Fem::FilteredGridPart
#include <dune/fem/gridpart/leafgridpart.hh>        // adds Dune::Fem::LeafGridPart
#include <dune/fem/io/file/dataoutput.hh>           // adds Dune::Fem::DataOutput
#include <dune/fem/io/parameter.hh>                 // adds Dune::Fem::Parameter
#include <dune/fem/misc/mpimanager.hh>              // adds Dune::Fem::MPIManager
#include <dune/fem/quadrature/cachingquadrature.hh> // adds Dune::Fem::CachingQuadrature
#include <dune/fem/space/common/adaptmanager.hh>    // adds Dune::Fem::GlobalRefine
#include <dune/fem/io/file/dataoutput.hh>

// local includes
#include "function.hh"                              // adds QuadraticFunction
#include "radialfilter.hh"                          // adds RadialFilter

// MyDataOutputParameters
// ----------------------

struct MyDataOutputParameters
: public Dune::Fem::LocalParameter< Dune::Fem::DataOutputParameters, MyDataOutputParameters >
{
  MyDataOutputParameters ( const int step )
  : step_( step )
  {}

  MyDataOutputParameters ( const MyDataOutputParameters &other )
  : step_( other.step_ )
  {}

  virtual int startcounter () const
  {
    return step_;
  }

private:
  int step_;
};

// compute the size of the domain and the average of the exact solution
template< class HGridType >
double algorithm ( HGridType &grid, int step )
{
  // return value is some meassure for the error
  double error = 0;

  //! [Setup scalar function space]
  // use a scalar function space
  typedef Dune::Fem::FunctionSpace< double, double, HGridType::dimensionworld, 1 > FunctionSpaceType;

  // construct an instance of the problem
  typedef QuadraticFunction< FunctionSpaceType > FunctionType;
  FunctionType function;
  //! [Setup scalar function space]

  // create grid part consisting of leaf level elements
  typedef Dune::Fem::LeafGridPart< HGridType > GridPartType;
  GridPartType gridPart( grid );

  /*********************************************************************
   *** TODO: Construct a filtered grid part                          ***
   *********************************************************************/

  // convert the continuously given problem into a grid function
  typedef Dune::Fem::GridFunctionAdapter< FunctionType, GridPartType > GridExactSolutionType;
  GridExactSolutionType exact( "exact solution", function, gridPart, 5 );

  /*********************************************************************
   *** TODO: iterate over the leaf elements of the grid to           ***
   ***       - compute the size of the domain                        ***
   ***       - compute the average of the analytic function given by ***
   ***         the problem variable                                  ***
   *********************************************************************/
  //! [Output the result for viewing]
  // setup tuple of functions to output (exact solution interpreted as a grid function)
  typedef Dune::tuple< const GridExactSolutionType * > IOTupleType;
  IOTupleType ioTuple( &exact );

  // write data
  typedef Dune::Fem::DataOutput< HGridType, IOTupleType > DataOutputType;
  DataOutputType dataOutput( grid, ioTuple, MyDataOutputParameters( step ) );
  dataOutput.write();
  //! [Output the result for viewing]

  // later we will return an approximation error
  return error;
}

// -------------
// Main function
// -------------

int main ( int argc, char **argv )
try
{
  //! [Initialization of MPI, Parameter, Grid]
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );
  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );
  // append default parameter file
  Dune::Fem::Parameter::append( "../data/parameter" );
  // type of hierarchical grid
  typedef Dune::GridSelector::GridType HGridType;
  //! [Initialization of MPI, Parameter, Grid]

  //! [Create macro grid]
  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey( HGridType::dimension );
  const std::string gridfile = Dune::Fem::Parameter::getValue< std::string >( gridkey );

  // the method rank and size from MPIManager are static
  if( Dune::Fem::MPIManager::rank() == 0 )
    std::cout << "Loading macro grid: " << gridfile << std::endl;

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr( gridfile );
  HGridType &grid = *gridPtr;

  // do initial load balance
  grid.loadBalance();
  //! [Create macro grid]

  //! [Reading parameters]
  // read parameter: initial grid refinement
  const int level = Dune::Fem::Parameter::getValue< int >( "intro.level" );
  // read parameter: number of EOC loops
  const int repeats = Dune::Fem::Parameter::getValue< int >( "intro.repeats", 0 );
  // number of global refinements to bisect grid width
  const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();
  // globally refine grid such that the grid width is halfed level times
  Dune::Fem::GlobalRefine::apply( grid, level * refineStepsForHalf );
  //! [Reading parameters]

  //! [Calculating solution and error]
  // calculate first step
  double oldError = algorithm( grid, 0 );

  for( int step = 1; step <= repeats; ++step )
  {
    // refine grid globally (such that grid width is halfed)
    // note: This function correctly adjusts memory for discrete functions, etc.
    Dune::Fem::GlobalRefine::apply( grid, refineStepsForHalf );

    const double newError = algorithm( grid, step );
    const double eoc = log( oldError / newError ) / M_LN2;
    if( Dune::Fem::MPIManager::rank() == 0 )
    {
      std::cout << "Error: " << newError << std::endl;
      std::cout << "EOC( " << step << " ) = " << eoc << std::endl;
    }
    oldError = newError;
  }
  //! [Calculating solution and error]

  return 0;
}
catch( const Dune::Exception &exception )
{
  // handle Dune exceptions by simply displaying their message
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}

