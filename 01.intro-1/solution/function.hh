#ifndef DUNE_FEM_SCHOOL_INTRO_FUNCTIONS_HH
#define DUNE_FEM_SCHOOL_INTRO_FUNCTIONS_HH

// dune-common includes
#include <dune/common/documentation.hh>

  // QuadraticFunction
  // -----------------

  /*
   * A simple example function
   */
  template< class FunctionSpace >
  class QuadraticFunction
  {
    typedef QuadraticFunction< FunctionSpace > ThisType;

    static const int dimDomain = FunctionSpace::dimDomain;
    static const int dimRange = FunctionSpace::dimRange;

  public:
    typedef FunctionSpace FunctionSpaceType;
    typedef typename FunctionSpaceType::DomainType DomainType;
    typedef typename FunctionSpaceType::RangeType RangeType;

    void evaluate ( const DomainType &x, RangeType &value ) const
    {
      value = RangeType( 0 );

      value[ 0 ] = 1;
      for( int i = 0; i < dimDomain; ++i )
        value[ 0 ] *= x[ i ]*x[ i ];
    }

  };

#endif // #ifndef DUNE_FEM_SCHOOL_INTRO_FUNCTIONS_HH
