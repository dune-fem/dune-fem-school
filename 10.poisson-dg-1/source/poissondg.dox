/** \page POISSON

    \subpage POISSONDG
    \page POISSONDG Discretizing the laplace operator using a DG method
    \tableofcontents
    
    \section POISSONDGintro Introduction
    In this part of the tutorial we show how to solve an elliptic equation
    using a Discontinuous Galerkin method (here we focus on the interior penalty (IP)
    approach). 

    We start from the elliptic operator developed in the previous tutorial \ref POISSON2
    The main work will be the construction of the spatial operator in
    \link 10.poisson-dg-1/source/dgelliptic.hh dgelliptic.hh \endlink. A few additional changes need to
    be made:
    - In \link 10.poisson-dg-1/source/femscheme.hh femscheme.hh \endlink we need to use a different
      \co{DiscreteFunctionSpace} - we use the \co{DiscontinuousGalerkinSpace} which is a 
      Discontinuous Galerkin space with orthonormalized monomialse as basis functions. 
      An alternative spaces is the
      \co{LagrangeDiscontinuousGalerkinSpace} using Lagrange basis function and
      on cube grids a further available space is the \co{LegendreDiscontinuousGalerkinSpace}
      basis on Legendre polynomials.
    - In \link 10.poisson-dg-1/source/dgrhs.hh dgrhs.hh \endlink we need to add the Dirichlet boundary conditions 
      during the assembly of the right hand side. Since this step has much in common with the
      assembly of the DG matrix, we do not describe that further here.
    - Finally for the IP method a penalty parameter \f$ \beta \f$ is needed which has to be large
      enough for the method to be stable. This is stored in the \co{Model} class and
      read from the \co{parameter} file using the token
      \co{dg.penalty}.
    .

    \section POISSONDGmethod The interior penalty method

    Given a test function
    \f$ v \f$ with support only on a given element \f$ T \f$
    bilinear form is
    \f{eqnarray*}
       \langle L[u],v\rangle &:=& \int_T D\nabla u_T\cdot\nabla v + muv \\
       && \quad  
         - \frac{1}{2}\int_{\partial T} (u_T-u^+)\nabla v\cdot n_T 
         - \frac{1}{2}\int_{\partial T} (\nabla u_T + \nabla u^+) \cdot n_T v 
         + \frac{\beta}{h} \int_{\partial T} (u_T - u^+)v
    \f}
    where \f$ u_T \f$ is the local function of \f$ u_h \f$ on \f$ T \f$,
    \f$ u^+ \f$ is the value of \f$ u_h \f$ on the elements neighboring \f$ T \f$,
    and \f$ n_T \f$ is the outer unit normal. The element integrals can be handled as 
    described in the tutorial \ref POISSON2 so we can focus on the boundary terms.

    \section POISSONDGoperator Applying the operator

    For the application of the operator we need to iterate over the intersections of a
    given element and setup a quadrature for that intersection. To evaluate local functions 
    on the entities on both sides of the intersection we use a
    \co{CachingQuadrature< GridPartType, 1 >}. This is a quadrature on the codimension
    zero reference element but with quadrature points on the codimension one entity in
    which the intersection lies. We need to setup two such quadratures with arguments
    \co{QuadratureType::INSIDE} and \co{QuadratureType::OUTSIDE} for the element
    \f$ T \f$ and its neighbor \f$ Y^+ \f$, respectively. 

    \snippet 10.poisson-dg-1/source/dgelliptic.hh Compute skeleton terms: iterate over intersections

    As with the quadratures on codimension zero entities the methods \co{point} and
    \co{weight} give the quadrature point in reference coordinates and the weight. In
    addition there is a method \co{localPoint} returning the quadrature point in the
    reference element of the intersection itself.

    \snippet 10.poisson-dg-1/source/dgelliptic.hh Compute skeleton terms: obtain required values on the intersection
    \image html intersectionquadrature.png "intersection quadrature"
    \image latex intersectionquadrature.eps "intersection quadrature" width=0.5\textwidth

    We again want to use the \co{axpy} method to add the local contribution to the
    function \f$ L[u_h] \f$. This requires combining all factors to be multiplied with
    the testfunction \f$ v \f$ and those to be multiplied with its gradient, i.e.,
    \f{eqnarray*}
       -\frac{1}{2} (\nabla u_T + \nabla u^+) \cdot n_T + \frac{\beta}{h} (u_T - u^+)~,
       \quad\mbox{and}\quad
       -\frac{1}{2} (u_T-u^+) n_T~,
    \f}
    respectively.

    \snippet 10.poisson-dg-1/source/dgelliptic.hh Compute skeleton terms: compute factors for axpy method

    \section POISSONDGmatrix Assembling the linear operator

    For the matrix assembly we want to follow the same lines as for the operator
    application. In contrast to the continuous finite-element approximation discussed in the
    tutorial \ref POISSON1, we now have contributions to the linear operator which
    combine basis functions with support on two different element. 

    \snippet 10.poisson-dg-1/source/dgelliptic.hh Assemble skeleton terms: get contributions on off diagonal block

    The off-diagonal block contains contributions of the form
    \f$ \langle L[w^+,v_T] \rangle \f$, i.e., all combinations of basis function on the element \f$ T \f$ in the second
    argument with basis function on neighboring elements in the first. We want to apply
    the \co{axpy} method for these contributions and for those on the diagonal blcoak,
    i.e., of the form \f$ \langle L[w_T,v_T]\rangle \f$. The factors for the later are
    \f{eqnarray*}
       -\frac{1}{2} \nabla w_T \cdot n_T + \frac{\beta}{h} w_T~,
       \quad\mbox{and}\quad
       -\frac{1}{2} u_T n_T~,
    \f}
    \f{eqnarray*}
       -\frac{1}{2} \nabla w_T\cdot n_T + \frac{\beta}{h} w_T ~,
       \quad\mbox{and}\quad
       -\frac{1}{2} w_T n_T~,
    \f}
    while for the former we need the factors
    \f{eqnarray*}
       -\frac{1}{2} \nabla w^+\cdot n_T - \frac{\beta}{h} w^+~,
       \quad\mbox{and}\quad
       \frac{1}{2} w^+ n_T~,
    \f}

    First we need to obtain the values of all the basis functions for a given element
    and apply the diffusion matrix \f$ D \f$:

    \snippet 10.poisson-dg-1/source/dgelliptic.hh Assemble skeleton terms: obtain values om quadrature point

    Then we compute the factors per column of each block (that is fixing the basis
    function in the first argument of the bilinear form):

    \snippet 10.poisson-dg-1/source/dgelliptic.hh Assemble skeleton terms: compute factors for axpy method

 **/

