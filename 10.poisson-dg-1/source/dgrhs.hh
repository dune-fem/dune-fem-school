#ifndef DGRHS_HH
#define DGRHS_HH

#include <dune/fem/quadrature/cachingquadrature.hh>

#include "rhs.hh"


// assembleRHS
// -----------

template< class Model, class Function, class DiscreteFunction >
void assembleDGRHS ( const Model &model, const Function &function, DiscreteFunction &rhs )
{
  assembleRHS( model, function, rhs );
}

#endif // #ifndef DGRHS_HH
