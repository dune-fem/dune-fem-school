#ifndef DGRHS_HH
#define DGRHS_HH

#include <dune/fem/quadrature/cachingquadrature.hh>

#include "rhs.hh"


// assembleRHS
// -----------

template< class Model, class Function, class Neuman, class Dirichlet, class DiscreteFunction >
void assembleDGRHS ( const Model &model, const Function &function, const Neuman &neuman, const Dirichlet &dirichlet, DiscreteFunction &rhs )
{
  assembleRHS( model, function, neuman, rhs );
  if ( ! model.hasDirichletBoundary() )
    return;

  typedef typename DiscreteFunction::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunction::LocalFunctionType LocalFunctionType;

  typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
  typedef typename IteratorType::Entity EntityType;
  typedef typename EntityType::Geometry GeometryType;
  typedef typename LocalFunctionType::RangeType RangeType;
  typedef typename LocalFunctionType::JacobianRangeType JacobianRangeType;
  typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
  static const int dimDomain = LocalFunctionType::dimDomain;
  static const int dimRange = LocalFunctionType::dimRange;

  typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
  typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType;
  typedef typename IntersectionIteratorType::Intersection IntersectionType;

  typedef Dune::Fem::ElementQuadrature< GridPartType, 1 > FaceQuadratureType;

  const DiscreteFunctionSpaceType &dfSpace = rhs.space();
  const int quadOrder = 2*dfSpace.order()+1;

  const IteratorType end = dfSpace.end();
  for( IteratorType it = dfSpace.begin(); it != end; ++it )
  {
    const EntityType &entity = *it;
    if ( !entity.hasBoundaryIntersections() )
      continue;

    const GeometryType &geometry = entity.geometry();
    double area = geometry.volume();

    LocalFunctionType rhsLocal = rhs.localFunction( entity );

    const IntersectionIteratorType iitend = dfSpace.gridPart().iend( entity );
    for( IntersectionIteratorType iit = dfSpace.gridPart().ibegin( entity ); iit != iitend; ++iit ) // looping over intersections
    {
      const IntersectionType &intersection = *iit;
      if ( ! intersection.boundary() ) // i.e. if intersection is on boundary: nothing to be done for Neumann zero b.c.
        continue;                      // since [u] = 0  and grad u.n = 0
      Dune::FieldVector<bool,dimRange> components(true);
      if ( ! model.isDirichletIntersection( intersection, components) )
        continue;

      const typename Dirichlet::LocalFunctionType dirichletLocal = dirichlet.localFunction( entity);

      typedef typename IntersectionType::Geometry  IntersectionGeometryType;
      const IntersectionGeometryType &intersectionGeometry = intersection.geometry();

      const double intersectionArea = intersectionGeometry.volume();
      const double beta = model.penalty() * intersectionArea / area;

      FaceQuadratureType quadInside( dfSpace.gridPart(), intersection, quadOrder, FaceQuadratureType::INSIDE );
      const size_t numQuadraturePoints = quadInside.nop();
      for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
      {
        const typename FaceQuadratureType::LocalCoordinateType &x = quadInside.localPoint( pt );
        const DomainType normal = intersection.integrationOuterNormal( x );

        const double weight = quadInside.weight( pt );

        RangeType value;
        JacobianRangeType dvalue,advalue;

        RangeType vuOut;
        dirichletLocal.evaluate( quadInside[pt], vuOut );
        for (int r=0;r<dimRange;++r)
          if (!components[r]) // do not use dirichlet constraints here
            vuOut[r] = 0;
        value = vuOut;
        value *= beta * intersectionGeometry.integrationElement( x );

        //  [ u ] * { grad phi_en } = -normal(u+ - u-) * 0.5 grad phi_en
	      // here we need a diadic product of u x n
        for (int r=0;r<dimRange;++r)
          for (int d=0;d<dimDomain;++d)
            dvalue[r][d] = -1.0 * normal[d] * vuOut[r];

        model.diffusiveFlux( entity, quadInside[ pt ], vuOut, dvalue, advalue );

        value *= weight;
        advalue *= weight;
        rhsLocal.axpy( quadInside[ pt ], value, advalue );
      }
    }
  }
  rhs.communicate();
}

#endif // #ifndef DGRHS_HH
