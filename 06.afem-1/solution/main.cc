#include <config.h>

// iostream includes
#include <iostream>
#include <complex>
#include <memory>

// include grid part
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>

// include output
#include <dune/fem/io/file/dataoutput.hh>

/*********************************************************/
/***                 NEW FOR LESSON 3                  ***/
/*********************************************************/
// include header of adaptive scheme
#include "afemscheme.hh"
/*********************************************************/

#include "poisson.hh"

// assemble-solve-estimate-mark-refine-IO-error-doitagain
template <class HGridType>
double algorithm ( HGridType &grid, int step )
{
  // we want to solve the problem on the leaf elements of the grid
  typedef Dune::Fem::AdaptiveLeafGridPart< HGridType, Dune::InteriorBorder_Partition > GridPartType;
  GridPartType gridPart(grid);

  // use a scalar function space
  typedef Dune::Fem::FunctionSpace< double, double,
              HGridType::dimensionworld, 2 > FunctionSpaceType;

  // type of the mathematical model used
  typedef DiffusionModel< FunctionSpaceType, GridPartType > ModelType;

  typedef typename ModelType::ProblemType ProblemType ;
  std::unique_ptr< ProblemType > problemPtr ;
  const std::string problemNames [] = { "cos", "sphere", "sin", "mixedbc", "corner", "curvedridges" };
  const int problemNumber = Dune::Fem::Parameter::getEnum("poisson.problem", problemNames, 0 );
  switch ( problemNumber )
  {
    case 0:
      problemPtr.reset( new CosinusProduct< FunctionSpaceType > () );
      break ;
    case 1:
      problemPtr.reset( new SphereProblem< FunctionSpaceType > () );
      break ;
    case 2:
      problemPtr.reset( new SinusProduct< FunctionSpaceType > () );
      break ;
    case 3:
      problemPtr.reset( new CosinusProductMixedBC< FunctionSpaceType > () );
      break ;
    case 4:
      problemPtr.reset( new ReentrantCorner< FunctionSpaceType > () );
      break ;
    case 5:
      problemPtr.reset( new CurvedRidges< FunctionSpaceType > () );
      break ;
    default:
      problemPtr.reset( new CosinusProduct< FunctionSpaceType > () );
  }
  assert( problemPtr );
  ProblemType& problem = *problemPtr ;

  // implicit model for left hand side
  ModelType implicitModel( problem, gridPart );

/*********************************************************/
/***                 NEW FOR LESSON 3                  ***/
/*********************************************************/
  // create adaptive scheme
  typedef AFemScheme< ModelType > SchemeType;
/*********************************************************/

  SchemeType scheme( gridPart, implicitModel );

  typedef Dune::Fem::GridFunctionAdapter< ProblemType, GridPartType > GridExactSolutionType;
  GridExactSolutionType gridExactSolution("exact solution", problem, gridPart, 5 );
  //! input/output tuple and setup datawritter
  typedef Dune::tuple< const typename SchemeType::DiscreteFunctionType *, GridExactSolutionType * > IOTupleType;
  typedef Dune::Fem::DataOutput< HGridType, IOTupleType > DataOutputType;
  IOTupleType ioTuple( &(scheme.solution()), &gridExactSolution) ; // tuple with pointers
  DataOutputType dataOutput( grid, ioTuple, DataOutputParameters( step ) );

  // setup the right hand side
  scheme.prepare();
  // solve once
  scheme.solve( true );

  // write initial solve
  dataOutput.write();

  // calculate error
  double error = 0 ;

/*********************************************************/
/***                 NEW FOR LESSON 3                  ***/
/*********************************************************/
  // step < 0 indicates the adaptive algorithm
  if( step < 0 )
  {
    //! [Adaptation cycle]
    // get tolerance for adaptive algorithm
    const double tolerance = Dune::Fem::Parameter::getValue< double >("adaptation.tolerance", 0.1);

    // get error estimator
    error = scheme.estimate();

    // until estimator is above tolerance for *THE LOOP*
    while ( error > tolerance )
    {
      // mark element for adaptation
      scheme.mark( tolerance );

      // adapt grid
      scheme.adapt();

      // assemble rhs and solve again
      scheme.prepare();
      scheme.solve( true );

      // data I/O
      dataOutput.write();

      // calculate new error
      error = scheme.estimate();
    }
    //! [Adaptation cycle]
  }
  else
/*********************************************************/

  {
    // calculate standard error

/*********************************************************/
/***                 NEW FOR LESSON 3                  ***/
/*********************************************************/
    // can also use H1-norm
    typedef Dune::Fem::H1Norm< GridPartType > NormType;
/*********************************************************/

    NormType norm( gridPart );
    return norm.distance( gridExactSolution, scheme.solution() );
  }

  return error ;
}

// main
// ----

int main ( int argc, char **argv )
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );

  // append possible given parameter files
  for( int i = 1; i < argc; ++i )
    Dune::Fem::Parameter::append( argv[ i ] );

  // append default parameter file
  Dune::Fem::Parameter::append( "../data/parameter" );

  // type of hierarchical grid
  typedef Dune::GridSelector::GridType  HGridType ;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey( HGridType::dimension );
  const std::string gridfile = Dune::Fem::Parameter::getValue< std::string >( gridkey );

  // the method rank and size from MPIManager are static
  if( Dune::Fem::MPIManager::rank() == 0 )
    std::cout << "Loading macro grid: " << gridfile << std::endl;

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr( gridfile );
  HGridType& grid = *gridPtr ;

  // do initial load balance
  grid.loadBalance();

  // initial grid refinement
  const int level = Dune::Fem::Parameter::getValue< int >( "poisson.level" );

  // number of global refinements to bisect grid width
  const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();

  // refine grid
  Dune::Fem::GlobalRefine::apply( grid, level * refineStepsForHalf );

  // setup EOC loop
  const int repeats = Dune::Fem::Parameter::getValue< int >( "poisson.repeats", 0 );

  // calculate first step
  double oldError = algorithm( grid, (repeats > 0) ? 0 : -1 );

  for( int step = 1; step <= repeats; ++step )
  {
    // refine globally such that grid with is bisected
    // and all memory is adjusted correctly
    Dune::Fem::GlobalRefine::apply( grid, refineStepsForHalf );

    const double newError = algorithm( grid, step );
    const double eoc = log( oldError / newError ) / M_LN2;
    if( Dune::Fem::MPIManager::rank() == 0 )
    {
      std::cout << "Error: " << newError << std::endl;
      std::cout << "EOC( " << step << " ) = " << eoc << std::endl;
    }
    oldError = newError;
  }

  return 0;
}
catch( const Dune::Exception &exception )
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}
