/*********************************************************/
/***                 NEW FOR LESSON 3                  ***/
/*********************************************************/
/**Residual estimator for Poisson's equation. Only for Dirichlet/periodic boundaries ATM. */
#ifndef ESTIMATOR_HH
#define ESTIMATOR_HH

//- Dune-fem includes
#include <dune/fem/misc/compatibility.hh>
#include <dune/fem/quadrature/caching/twistutility.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/quadrature/intersectionquadrature.hh>
#include <dune/fem/operator/common/spaceoperatorif.hh>
#include <dune/fem/operator/matrix/blockmatrix.hh>

// Estimator
// ---------
template< class DiscreteFunction >
class Estimator
{
  typedef Estimator< DiscreteFunction > ThisType;

public:
  typedef DiscreteFunction DiscreteFunctionType;

  typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType
    DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionType :: LocalFunctionType LocalFunctionType;

  typedef typename DiscreteFunctionSpaceType :: DomainFieldType DomainFieldType;
  typedef typename DiscreteFunctionSpaceType :: RangeFieldType RangeFieldType;
  typedef typename DiscreteFunctionSpaceType :: DomainType DomainType;
  typedef typename DiscreteFunctionSpaceType :: RangeType RangeType;
  typedef typename DiscreteFunctionSpaceType :: JacobianRangeType JacobianRangeType;
  typedef typename DiscreteFunctionSpaceType :: GridPartType GridPartType;
  typedef typename DiscreteFunctionSpaceType :: IteratorType IteratorType;

  typedef typename GridPartType :: GridType GridType;
  typedef typename GridPartType :: IndexSetType IndexSetType;
  typedef typename GridPartType :: IntersectionIteratorType IntersectionIteratorType;

  typedef typename IntersectionIteratorType :: Intersection IntersectionType;

  typedef typename GridType :: template Codim< 0 > :: Entity ElementType;
  typedef typename ElementType::Geometry GeometryType;
  static const int dimension = GridType :: dimension;

  typedef Dune::Fem::CachingQuadrature< GridPartType, 0 > ElementQuadratureType;
  typedef Dune::Fem::CachingQuadrature< GridPartType, 1 > FaceQuadratureType;

  //typedef Dune::ElementQuadrature< GridPartType, 0 > ElementQuadratureType;
  //typedef Dune::ElementQuadrature< GridPartType, 1 > FaceQuadratureType;

  typedef Dune::FieldMatrix<double,dimension,dimension> JacobianInverseType;
  typedef std :: vector< double > ErrorIndicatorType;

private:
  const DiscreteFunctionType &uh_;
  const DiscreteFunctionSpaceType &dfSpace_;
  GridPartType &gridPart_;
  const IndexSetType &indexSet_;
  GridType &grid_;
  ErrorIndicatorType indicator_;

public:
  static_assert( static_cast< unsigned int >( GridType::dimension ) == static_cast< unsigned int >( GridType::dimensionworld ),
                 "the estimator is not implemented for surfaces problems" );

  explicit Estimator ( const DiscreteFunctionType &uh )
  : uh_( uh ),
    dfSpace_( uh.space() ),
    gridPart_( dfSpace_.gridPart() ),
    indexSet_( gridPart_.indexSet() ),
    grid_( gridPart_.grid() ),
    indicator_( indexSet_.size( 0 ) )
  {}

  //! calculate estimator
  template< class RHSFunctionType >
  double estimate ( const RHSFunctionType &rhs )
  {
    // clear all local estimators
    clear();

    //! [Error estimator]
    // calculate local estimator
    const IteratorType end = dfSpace_.end();
    for( IteratorType it = dfSpace_.begin(); it != end; ++it )
      estimateLocal( rhs, *it );

    double error = 0.0;

    // sum up local estimators
    const typename ErrorIndicatorType :: const_iterator endEstimator = indicator_.end();
    for( typename ErrorIndicatorType :: const_iterator it = indicator_.begin();
          it != endEstimator; ++it )
      error += *it;

    // obtain global sum
    error = grid_.comm().sum( error );

    // print error estimator
    std :: cout << "Estimated H1-Error: " << sqrt( error ) << std :: endl;
    return sqrt( error );
    //! [Error estimator]
  }

  //! mark all elements due to given tolerance
  bool mark ( const double tolerance ) const
  {
    // possible strategies
    enum Strategy { none=0, maximum=1, equiv=2, uniform=3 };
    static const std::string strategyNames []
          = { "none", "maximum", "equidistribution", "uniform" };
    Strategy strategy = (Strategy) Dune::Fem::Parameter :: getEnum("adaptation.strategy", strategyNames );

    double localTol2 = 0;
    switch( strategy )
    {
    case maximum:
      {
        double maxError = 0 ;
        // sum up local estimators
        const typename ErrorIndicatorType :: const_iterator endEstimator = indicator_.end();
        for( typename ErrorIndicatorType :: const_iterator it = indicator_.begin();
              it != endEstimator; ++it )
        {
          maxError = std::max( *it, maxError );
        }
        // get global maxError
        maxError = grid_.comm().max( maxError );
        localTol2 = 0.25 * maxError ;
      }

      break ;
    case equiv:
      {
        double sumError = 0 ;
        // sum up local estimators
        const size_t indSize = indicator_.size();
        for( size_t i=0; i<indSize; ++i)
        {
          sumError += indicator_[ i ];
        }

        // get global sum of number of elements and local error sum
        double buffer[ 2 ] = { (double)indexSet_.size( 0 ) , sumError };
        grid_.comm().sum( buffer, 2 );
        localTol2 = 0.95 * tolerance*tolerance * buffer[ 1 ] / buffer[ 0 ];
      }
      break ;
    default:
      break ;
    }

    //! [Mark entities]
    int marked = 0;
    // loop over all elements
    const IteratorType end = dfSpace_.end();
    for( IteratorType it = dfSpace_.begin(); it != end; ++it )
    {
      const ElementType &entity = *it;
      // check local error indicator
      if( indicator_[ indexSet_.index( entity ) ] > localTol2 )
      {
        // mark entity for refinement
        grid_.mark( 1, entity );
        // grid was marked
        marked = 1;
      }
    }

    // get global max
    marked = grid_.comm().max( marked );
    //! [Mark entities]
    return bool(marked);
  }

protected:
  void clear ()
  {
    // resize and clear
    indicator_.resize( indexSet_.size( 0 ) );
    typedef typename ErrorIndicatorType :: iterator IteratorType;
    const IteratorType end = indicator_.end();
    for( IteratorType it = indicator_.begin(); it != end; ++it )
      *it = 0.0;
  }

  //! caclulate error on element
  template< class RHSFunctionType >
  void estimateLocal ( const RHSFunctionType &rhs, const ElementType &entity )
  {
    const typename ElementType :: Geometry &geometry = entity.geometry();

    const double volume = geometry.volume();
    const double h2 = (dimension == 2 ? volume : std :: pow( volume, 2.0 / (double)dimension ));
    const int index = indexSet_.index( entity );
    const LocalFunctionType uLocal = uh_.localFunction( entity );
    typename RHSFunctionType::LocalFunctionType localRhs = rhs.localFunction( entity );

    ElementQuadratureType quad( entity, 2*(dfSpace_.order() + 1) );

    // compute element residual
    const int numQuadraturePoints = quad.nop();
    for( int qp = 0; qp < numQuadraturePoints; ++qp )
    {
      const typename ElementQuadratureType :: CoordinateType &x = quad.point( qp );
      const double weight = quad.weight( qp ) * geometry.integrationElement( x );

      RangeType y;
      localRhs.evaluate( quad[qp], y );

      RangeType tmp;
      laplacianLocal( uLocal, quad[ qp ], tmp );
      y += tmp;

      indicator_[ index ] += h2 * weight * y.two_norm2();
    }

    // calculate face contribution
    IntersectionIteratorType end = gridPart_.iend( entity );
    for( IntersectionIteratorType it = gridPart_.ibegin( entity ); it != end; ++it )
    {
      const IntersectionType &intersection = *it;
      // if we got an element neighbor
      if( intersection.neighbor() )
        estimateIntersection( intersection, entity, uLocal );
    }
  }

  //! caclulate error on boundary intersections
  void estimateIntersection ( const IntersectionType &intersection,
                              const ElementType &inside,
                              const LocalFunctionType &uInside )
  {
    const ElementType outside = intersection.outside();

    const int insideIndex = indexSet_.index( inside );
    const int outsideIndex = indexSet_.index( outside );

    // only compute intersection estimator once - either if the
    // outside entity has a larger index than the inside entity or
    // the intersection is on the boundary (parallel case...)
    const bool isOutsideInterior = (outside.partitionType() == Dune::InteriorEntity);
    if( !isOutsideInterior || (insideIndex < outsideIndex) )
    {
      const LocalFunctionType uOutside = uh_.localFunction( outside );

      double error;

      if( intersection.conforming() )
        error = estimateIntersection< true >( intersection, uInside, uOutside );
      else
        error = estimateIntersection< false >( intersection, uInside, uOutside );

      if( error > 0.0 )
      {
        const double volume
          = 0.5 * (inside.geometry().volume() + outside.geometry().volume());
        const double h = (dimension == 1 ? volume : std::pow( volume, 1.0 / (double)dimension ));
        indicator_[ insideIndex ] += h * error;
        if( isOutsideInterior )
          indicator_[ outsideIndex ] += h * error;
      }
    }
  }

  //! caclulate error on element intersections
  template< bool conforming  >
  double estimateIntersection ( const IntersectionType &intersection,
                                const LocalFunctionType &uInside,
                                const LocalFunctionType &uOutside ) const
  {
    // make sure correct method is called
    assert( intersection.conforming() == conforming );

    // use IntersectionQuadrature to create appropriate face quadratures
    typedef Dune :: Fem :: IntersectionQuadrature< FaceQuadratureType, conforming > IntersectionQuadratureType;
    typedef typename IntersectionQuadratureType :: FaceQuadratureType Quadrature ;

    const int quadOrder = 2 * (dfSpace_.order() - 1);
    // create intersection quadrature
    IntersectionQuadratureType intersectionQuad( gridPart_, intersection, quadOrder );

    // get appropriate quadrature references
    const Quadrature &quadInside  = intersectionQuad.inside();
    const Quadrature &quadOutside = intersectionQuad.outside();

    double error = 0.0;
    /**********************************************************************
     *** TODO: implement error estimator for the intersection part      ***
     ***       loop over all quadrature point and evaluate jacobian of  ***
     ***       uInside and uOutside                                     ***
     **********************************************************************/
    return error;
  }

  template< class PointType >
  void laplacianLocal ( const LocalFunctionType &u_h,
                        const PointType &x, RangeType &result ) const
  {
    typename LocalFunctionType::HessianRangeType hessian;
    u_h.hessian( x, hessian );

    result = 0;
    for( int r = 0; r < LocalFunctionType::dimRange; ++r )
    {
      for( int i = 0; i < dimension; ++i )
        result[ r ] += hessian[ r ][ i ][ i ];
    }
  }
};

#endif // #ifndef ESTIMATOR_HH
/*********************************************************/

