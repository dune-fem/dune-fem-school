/*********************************************************/
/***                 NEW FOR LESSON 3                  ***/
/*********************************************************/

#ifndef ELLIPT_ADAPTSCHEME_HH
#define ELLIPT_ADAPTSCHEME_HH

#include "femscheme.hh"
#include "estimator.hh"

// AFemScheme
//------------------

template < class Model >
struct AFemScheme : public FemScheme<Model>
{
  typedef FemScheme<Model> BaseType;
  typedef typename BaseType::GridType GridType;
  typedef typename BaseType::GridPartType GridPartType;
  typedef typename BaseType::ModelType ModelType;
  typedef typename BaseType::FunctionSpaceType FunctionSpaceType;
  typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;
  //! type of restriction/prolongation projection for adaptive simulations
  //! (use default here, i.e. LagrangeInterpolation)
  typedef Dune::Fem::RestrictProlongDefault< DiscreteFunctionType >  RestrictionProlongationType;

  //! type of adaptation manager handling adapation and DoF compression
  typedef Dune::Fem::AdaptationManager< GridType, RestrictionProlongationType > AdaptationManagerType;

  //! type of error estimator
  typedef Estimator< DiscreteFunctionType > EstimatorType ;

  AFemScheme( GridPartType &gridPart,
              const ModelType& implicitModel)
  : BaseType(gridPart,implicitModel),
      // estimator
      estimator_( solution_ ),
      // restriction/prolongation operator
      restrictProlong_( solution_ ),
      // adaptation manager
      adaptationManager_( gridPart_.grid(), restrictProlong_ )
  {}

  //! mark elements for adaptation
  bool mark ( const double tolerance )
  {
    return estimator_.mark( tolerance );
  }

  //! calculate error estimator
  double estimate()
  {
    return estimator_.estimate( this->implicitModel_.rightHandSide() );
  }

  //! do the adaptation for a given marking
  void adapt()
  {
    // apply adaptation and load balancing
    adaptationManager_.adapt();
  }

protected:
  EstimatorType  estimator_;  // residual error estimator

  RestrictionProlongationType restrictProlong_ ; // local restriction/prolongation object
  AdaptationManagerType  adaptationManager_ ;    // adaptation manager handling adaptation

  using BaseType::gridPart_;
  using BaseType::discreteSpace_;
  using BaseType::solution_;

};

#endif // end #if ELLIPT_FEMSCHEME_HH
/*********************************************************/

