/*********************************************************/
/***                 NEW FOR LESSON 3                  ***/
/*********************************************************/

#ifndef SPIRAL_MODEL_HH
#define SPIRAL_MODEL_HH

#include <cassert>
#include <cmath>

#include <dune/fem/solver/timeprovider.hh>
#include <dune/fem/io/parameter.hh>

#include "temporalprobleminterface.hh"
#include "model.hh"

template <class FunctionSpace>
class SpiralProblem : public TemporalProblemInterface < FunctionSpace >
{
  typedef TemporalProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  typedef Dune::Fem::TimeProviderBase TimeProviderType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  // get time function from base class
  using BaseType :: time ;

  SpiralProblem( const TimeProviderType &timeProvider )
    : BaseType( timeProvider )
  {}

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {
    phi = RangeType(0);
  }

  //! the exact solution
  virtual void u(const DomainType& x,
                 RangeType& phi) const
  {
    phi[ 0 ] = x[1]>.01?1:0;
    phi[ 1 ] = x[0]<0?.5:0;
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    ret = JacobianRangeType(0);
  }
};
template< class FunctionSpace, class GridPart >
struct SpiralModel : public DiffusionModel<FunctionSpace,GridPart>
{
  typedef DiffusionModel<FunctionSpace,GridPart> BaseType;
  typedef FunctionSpace FunctionSpaceType;
  typedef GridPart GridPartType;

  typedef TemporalProblemInterface< FunctionSpaceType > ProblemType ;
  typedef typename BaseType::ProblemType InitialFunctionType;

  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

  typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
  typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

  typedef Dune::Fem::TimeProviderBase TimeProviderType;

  static const int dimRange = FunctionSpaceType::dimRange;

  //! constructor taking problem reference, time provider,
  //! time step factor( either theta or -(1-theta) ),
  //! flag for the right hand side
  SpiralModel( const ProblemType& problem,
               const GridPart &gridPart,
               const bool implicit )
    : BaseType(problem,gridPart),
      timeProvider_(problem.timeProvider()),
      implicit_(implicit),
      b(1./100.), c(3./4.), epsInv(50.), diffusion(1./100.)
  {
  }

  //! this model does not have any mass term
  template< class Entity, class Point >
  void source ( const Entity &entity,
                const Point &x,
                const RangeType &value,
                const JacobianRangeType &gradient,
                RangeType &flux ) const
  {
    flux = 0;

    if( !implicit_ )
    {
      double uth = (value[1]+b)/c;
      if ( value[0] <= uth )
        flux[ 0 ] = 0;
      else
        flux[ 0 ] = epsInv*value[0]*(value[0]-uth);
      flux[ 1 ] = value[0]-value[1];
    }
    else
    {
      double uth = (value[1]+b)/c;
      if ( value[0] <= uth )
        flux[ 0 ] = -epsInv*value[0]*(1.-value[0])*(value[0]-uth);
      else
        flux[ 0 ] =  epsInv*value[0]*value[0]*(value[0]-uth);
    }
    flux *= timeProvider().deltaT();
    // time diescretization
    flux += value;
  }
  //! this model does not have any mass term
  template< class Entity, class Point >
  void linSource ( const RangeType& uBar,
                   const JacobianRangeType &gradientBar,
                   const Entity &entity,
                   const Point &x,
                   const RangeType &value,
                   const JacobianRangeType &gradient,
                   RangeType &flux ) const
  {
    flux = 0;

    if( !implicit_ )
    {
      std::cout << "this should not be called for the explicit part of the model - aborting..." << std::endl;
      abort();
      double uth = (uBar[1]+b)/c;
      if ( uBar[0] <= uth )
        flux[ 0 ] = 0;
      else
        flux[ 0 ] = epsInv*value[0]*(uBar[0]-uth);
      flux[ 1 ] = uBar[0]-uBar[1];
    }
    else
    {
      double uth = (uBar[1]+b)/c;
      if ( uBar[0] <= uth )
        flux[ 0 ] = -epsInv*value[0]*(1.-uBar[0])*(uBar[0]-uth);
      else
        flux[ 0 ] =  epsInv*value[0]*uBar[0]*(uBar[0]-uth);
    }
    flux *= timeProvider().deltaT();
    // time diescretization
    flux += value;
  }

  //! return the diffusive flux
  template< class Entity, class Point >
  void diffusiveFlux ( const Entity &entity,
                       const Point &x,
                       const RangeType &value,
                       const JacobianRangeType &gradient,
                       JacobianRangeType &flux ) const
  {
    if (implicit_)
    {
      flux[ 0 ] = gradient[ 0 ];
      flux[ 1 ] = 0;
      flux *= diffusion*timeProvider().deltaT();
    }
    else
    {
      flux = 0.;
    }
  }
  //! return the diffusive flux
  template< class Entity, class Point >
  void linDiffusiveFlux ( const RangeType& uBar,
                          const JacobianRangeType &gradientBar,
                          const Entity &entity,
                          const Point &x,
                          const RangeType &value,
                          const JacobianRangeType &gradient,
                          JacobianRangeType &flux ) const
  {
    diffusiveFlux(entity,x,value,gradient,flux);
  }

  //! return reference to Problem's time provider
  const TimeProviderType & timeProvider() const
  {
    return timeProvider_;
  }

  bool hasDirichletBoundary () const
  {
    return false;
  }
  template <class Intersection>
  bool isDirichletIntersection( const Intersection& inter, Dune::FieldVector<bool,dimRange> &dirichletComponent ) const
  {
    return false;
  }
  const InitialFunctionType &initialFunction() const
  {
    return problem_;
  }

   //penalty parameter
  double penalty() const
  {
    return BaseType::penalty()*diffusion;
  }
protected:
  const TimeProviderType &timeProvider_;
  double timeStepFactor_;
  using BaseType::problem_;
  bool implicit_;
  const double b,c,epsInv,diffusion;
};
#endif // #ifndef HEAT_MODEL_HH

/*********************************************************/

