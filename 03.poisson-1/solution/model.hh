#ifndef ELLIPTC_MODEL_HH
#define ELLIPTC_MODEL_HH

#include <cassert>
#include <cmath>

#include <dune/fem/solver/timeprovider.hh>
#include <dune/fem/io/parameter.hh>

#include "probleminterface.hh"
#include "modelinterface.hh"

/********************************************
  Full non-linar elliptic model

  struct EllipticModel
  {
    //! [Methods used for operator application]
    template< class Entity, class Point >
    void source ( const Entity &entity, const Point &x,
                  const RangeType &value, const JacobianRangeType &gradient,
                  RangeType &flux ) const;
    template< class Entity, class Point >
    void diffusiveFlux ( const Entity &entity, const Point &x,
                         const RangeType &value, const JacobianRangeType &gradient,
                         JacobianRangeType &flux ) const;
    //! [Methods used for operator application]

    //! [Methods used to assemble linearized operator]
    template< class Entity, class Point >
    void linSource ( const RangeType& uBar, const JacobianRangeType& gradientBar,
                     const Entity &entity, const Point &x,
                     const RangeType &value, const JacobianRangeType &gradient,
                     RangeType &flux ) const;
    template< class Entity, class Point >
    void linDiffusiveFlux ( const RangeType& uBar, const JacobianRangeType& gradientBar,
                            const Entity &entity, const Point &x,
                            const RangeType &value, const JacobianRangeType &gradient,
                            JacobianRangeType &flux ) const;
    //! [Methods used to assemble linearized operator]

    //! [Methods used for Dirichlet constraints]
    bool hasDirichletBoundary () const;
    template <class Intersection>
    bool isDirichletIntersection( const Intersection& inter, Dune::FieldVector<bool,dimR>& component ) const;
    DirichletBoundaryType dirichletBoundary( ) const;
    bool hasNeumanBoundary () const;
    NeumanBoundaryType neumanBoundary( ) const;
    template< class Entity, class Point >
    void alpha(const Entity &entity, const Point &x, const RangeType &value, RangeType &val);
    void linAlpha(const RangeType &ubar, const Entity &entity, const Point &x, const RangeType &value, RangeType &val);
    //! [Methods used for Dirichlet constraints]
  };

 ********************************************/

// DiffusionModel
// --------------

template< class FunctionSpace, class GridPart >
struct DiffusionModel
{
  typedef FunctionSpace FunctionSpaceType;
  typedef GridPart GridPartType;

  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

  typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
  typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

  typedef ProblemInterface< FunctionSpaceType > ProblemType ;

  static const int dimRange = FunctionSpaceType::dimRange;

protected:
  enum FunctionId { rhs, bndD, bndN };
  template <FunctionId id>
  class FunctionWrapper;
public:
  typedef Dune::Fem::GridFunctionAdapter< FunctionWrapper<rhs>, GridPartType > RightHandSideType;
  typedef Dune::Fem::GridFunctionAdapter< FunctionWrapper<bndD>, GridPartType > DirichletBoundaryType;
  typedef Dune::Fem::GridFunctionAdapter< FunctionWrapper<bndN>, GridPartType > NeumanBoundaryType;

  //! constructor taking problem reference
  DiffusionModel( const ProblemType& problem, const GridPart &gridPart )
    : problem_( problem ),
      gridPart_(gridPart),
      rhs_(problem_),
      bndD_(problem_),
      bndN_(problem_)
  {
  }

  template< class Entity, class Point >
  void source ( const Entity &entity,
                const Point &x,
                const RangeType &value,
                const JacobianRangeType &gradient,
                RangeType &flux ) const
  {
    const DomainType xGlobal = entity.geometry().global( coordinate( x ) );
    RangeType m;
    problem_.m(xGlobal,m);
    for (unsigned int i=0;i<flux.size();++i)
      flux[i] = m[i]*value[i];
  }

  //! return the diffusive flux
  template< class Entity, class Point >
  void diffusiveFlux ( const Entity &entity,
                       const Point &x,
                       const RangeType &value,
                       const JacobianRangeType &gradient,
                       JacobianRangeType &flux ) const
  {
    // the flux is simply the identity
    flux = gradient;
  }

  // return Fem :: Function for right hand side
  RightHandSideType rightHandSide(  ) const
  {
    return RightHandSideType( "right hand side", rhs_, gridPart_, 5 );
  }

protected:
  template <FunctionId id>
  class FunctionWrapper : public Dune::Fem::Function< FunctionSpaceType, FunctionWrapper< id > >
  {
    const ProblemInterface<FunctionSpaceType>& impl_;
    public:
    FunctionWrapper( const ProblemInterface<FunctionSpaceType>& impl )
    : impl_( impl ) {}

    //! evaluate function
    void evaluate( const DomainType& x, RangeType& ret ) const
    {
      if( id == rhs )
      {
        // call right hand side of implementation
        impl_.f( x, ret );
      }
      else
      {
        DUNE_THROW(Dune::NotImplemented,"FunctionId not implemented");
      }
    }
  };

  const ProblemType& problem_;
  const GridPart &gridPart_;
  FunctionWrapper<rhs> rhs_;
  FunctionWrapper<bndD> bndD_;
  FunctionWrapper<bndN> bndN_;
};

#endif // #ifndef ELLIPTC_MODEL_HH
