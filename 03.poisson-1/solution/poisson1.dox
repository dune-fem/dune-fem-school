/** \page POISSON Solving an elliptic operation
    
    \subpage POISSON1
    \page POISSON1 Matrix free Laplace operator

  In this section we will build on the concepts described in the
  \ref INTRO2 "previous example" to assemble a functional. We will use this to
  solve the elliptic problem
  \f{eqnarray*}
    -\nabla\cdot D(x)\nabla u(x) + m(x)u(x) &=& f(x)
      \quad x\in\Omega \\
    D(x)\nabla u(x)\cdot \nu &=& 0
      \quad x\in\partial\Omega
  \f}
  without assembling a matrix, i.e., we use a matrix free CG solver available in
  dune-fem. This type of Krylov solver only requires that one implements the application
  of the linear operator  \f$ L[u] \f$ mapping from a Lagrange space
  \f$ V_h \f$ into its dual. 
  
  \note This is also the first step required to implement
  a Newton method for a non-linear problem. 
  
  As described in the
  \ref INTRO2 "previous example" applying the operator to a given \f$ u \f$ is
  equivalent to assembling a functional since
  \f[
   \langle L[u],v\rangle := \int_\Omega D\nabla u\cdot\nabla v + muv \quad v\in V_h
  \f]
  The same strategy can be used as for the right hand side assembly.

  The implementation is carried out in the \link 03.poisson-1/solution/elliptic.hh elliptic.hh\endlink
  header file. The operator class derives from the \class{Fem::Operator} base class:

  \snippet 03.poisson-1/solution/elliptic.hh Class for elliptic operator

  The first template argument defines the Lagrange space used (set up in the \class{Scheme} class
  defined in \link 03.poisson-1/solution/femscheme.hh femscheme.hh\endlink).
  The second template argument is the \class{Model}
  class which contains the method describing the data functions \f$ D,m \f$. This class must contain
  the following two methods:

  \snippet 03.poisson-1/solution/model.hh Methods used for operator application

  The \func{source()} method will always return all parts of the operator multiplied with \f$ v \f$,
  e.g., \f$ m(x)u(x) \f$.
  The \func{diffusionFlux()} returns all terms multiplied with \f$ \nabla v \f$,
  e.g., \f$ D(x)\nabla u(x) \f$.
  
  \note Both methods are setup to be used with a non linear elliptic problem later on but for
  this example the dependency on the \f$ u,\nabla u \f$ parameter must be linear.

  Classes deriving from the \class{Fem::Operator} interface class must implement a virtual
  \code
    virtual void operator() ( const DiscreteFunctionType &u, DiscreteFunctionType &w ) const;
  \endcode
  method. This takes the discrete function \f$ u \f$ as argument and assembles the functional
  \f$ w=L[u] \f$. The main step is the computation of the contribution to this functional on each
  element \f$ E \f$ of the grid. So within an iteration over the grid we have code of the form, giving
  us a quadrature on a given element. Here we need to compute the local contributions.
  \f{eqnarray*}
    w_{E,i} := \langle L[u_E],\varphi_i \rangle := \int_E D\nabla u_E\cdot\nabla  \varphi_i + mu_E\varphi_i
  \f}
  where \f$ \varphi_i \f$ are the basis functions of \f$ V_h \f$ with support on
  \f$ E \f$. The function \f$ u_E \f$ is the description of \f$ u \f$ restricted to
  the element \f$ E \f$.

  To compute \f$ D\nabla u_E(x_\alpha)\cdot\nabla \varphi_i(x_\alpha) + m(x_\alpha)u_E(x_\alpha)\varphi_i(x_\alpha) \f$
  at a given quadrature point \f$ x_\alpha \f$ (multiplied with a given quadrature weight):

  \snippet 03.poisson-1/solution/elliptic.hh Compute local contribution of operator

  The details of this code snippet were described in the previous example. 
  Recall the definition of
  the \func{axpy()} method, where we are using a further variant of this method, which
  takes care of
  \f$ {\rm adu}\; \cdot\;\nabla \varphi_i(x_\alpha) + {\rm avu}\; \varphi_i(x_\alpha) \f$

  After calling the \func{communicate()} method to distribute the result between different
  processors in the case of a parallel simulation, the computation is completed.

  \sa Assembling the right hand side functional and the details of the \class{Scheme}
  class are described in the \ref INTRO2 example. 
  
  Also \link 03.poisson-1/solution/main.cc main.cc\endlink has not
  really changed.
**/
