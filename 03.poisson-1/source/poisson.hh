#ifndef POISSON_PROBLEMS_HH
#define POISSON_PROBLEMS_HH

#include <cassert>
#include <cmath>

#include "probleminterface.hh"

// -laplace u + u = f with Neumann boundary conditions on domain [0,1]^d
// Exsct solution is u(x_1,...,x_d) = cos(2*pi*x_1)...cos(2*pi*x_d)
template <class FunctionSpace>
class CosinusProduct : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {
    phi = 4*dimDomain*(M_PI*M_PI);
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::cos( 2*M_PI*x[ i ] );
    RangeType uVal;
    u(x,uVal);
    phi += uVal;
  }

  //! the exact solution
  virtual void u(const DomainType& x,
                 RangeType& phi) const
  {
    phi = 1;
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::cos( 2*M_PI*x[ i ] );
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    for( int r = 0; r < dimRange; ++ r )
    {
      for( int i = 0; i < dimDomain; ++i )
      {
        ret[ r ][ i ] = -2*M_PI*std::sin( 2*M_PI*x[ i ] );
        for( int j = 1; j < dimDomain; ++j )
          ret[ r ][ i ] *= std::cos( 2*M_PI*x[ (i+j)%dimDomain ] );
      }
    }
  }
  //! mass coefficient has to be 1 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = RangeType(1);
  }
};
// -laplace u = f with zero Dirichlet boundary conditions on domain [0,1]^d
// Exsct solution is u(x_1,...,x_d) = sin(2*pi*x_1)...sin(2*pi*x_d)
template <class FunctionSpace>
class SinusProduct : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {
    phi = 4.*dimDomain*(M_PI*M_PI);
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::sin( 2*M_PI*x[ i ] );
  }

  //! the exact solution
  virtual void u(const DomainType& x,
                 RangeType& phi) const
  {
    phi = 1;
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::sin( 2.*M_PI*x[ i ] );
    phi[0] += x[0]*x[0]-x[1]*x[1]+x[0]*x[1];
    // phi[0] += x[0]*x[1];
    // phi[0] += 0.5;
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    for( int r = 0; r < dimRange; ++ r )
    {
      for( int i = 0; i < dimDomain; ++i )
      {
        ret[ r ][ i ] = 2*M_PI*std::cos( 2*M_PI*x[ i ] );
        for( int j = 1; j < dimDomain; ++j )
          ret[ r ][ i ] *= std::sin( 2*M_PI*x[ (i+j)%dimDomain ] );
      }
    }
    ret[0][0] +=  2.*x[0]+x[1];
    ret[0][1] += -2.*x[1]+x[0];
    // ret[0][0] += x[1];
    // ret[0][1] += x[0];
  }
  //! mass coefficient has to be 1 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = RangeType(0);
  }

  //! return true if given point belongs to the Dirichlet boundary (default is true)
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    return true;
  }
  virtual bool hasDirichletBoundary () const
  {
    return true ;
  }
};
template <class FunctionSpace>
class CosinusProductMixedBC : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& phi) const
  {
    phi = 4*dimDomain*(M_PI*M_PI);
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::cos( 2*M_PI*x[ i ] + 1. );
  }

  //! the exact solution
  virtual void u(const DomainType& x,
                 RangeType& phi) const
  {
    phi = 1;
    for( int i = 0; i < dimDomain; ++i )
      phi *= std::cos( 2*M_PI*x[ i ] + 1. );
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    for( int r = 0; r < dimRange; ++ r )
    {
      for( int i = 0; i < dimDomain; ++i )
      {
        ret[ r ][ i ] = -2*M_PI*std::sin( 2*M_PI*x[ i ] +1. );
        for( int j = 1; j < dimDomain; ++j )
          ret[ r ][ i ] *= std::cos( 2*M_PI*x[ (i+j)%dimDomain ] +1. );
      }
    }
  }

  //! mass coefficient can be 0 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = RangeType(0);
  }
  virtual void alpha(const DomainType& x, RangeType &a) const
  {
    a = RangeType(0.5);
  }
  //! the Dirichlet boundary data (default calls u)
  virtual void g(const DomainType& x,
                 RangeType& value) const
  {
    u(x,value);
  }
  virtual bool hasDirichletBoundary () const
  {
    return true ;
  }
  virtual bool hasNeumanBoundary () const
  {
    return true ;
  }
  virtual bool isDirichletPoint( const DomainType& x ) const
  {
    // all boundaries except the x=0 plane are Dirichlet
    return (std::abs(x[0])>1e-8);

  }
  virtual void n(const DomainType& x,
                 RangeType& value) const
  {
    u(x,value);
    value *= 0.5;
    JacobianRangeType jac;
    uJacobian(x,jac);
    value[0] -= jac[0][0];
  }
};

// A problem on a unit sphere
template <class FunctionSpace>
class SphereProblem : public ProblemInterface < FunctionSpace >
{
  typedef ProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;
  typedef typename BaseType :: DiffusionTensorType  DiffusionTensorType;

  typedef typename FunctionSpace::HessianRangeType HessianRangeType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  //! the right hand side data (default = 0)
  virtual void f(const DomainType& x,
                 RangeType& ret) const
  {
    DomainType y = x;
    y /= x.two_norm();

    JacobianRangeType Ju;
    uJacobian( y, Ju );
    Ju.mv( y, ret );
    ret *= double(dimDomain - 1);

#if 0
    HessianRangeType Hu;
    hessian( y, Hu );
    for( int i = 0; i < dimDomain; ++i )
    {
      DomainType ds = y;
      ds *= -y[ i ];
      ds[ i ] += 1;

      for( int j = 0; j < dimRange; ++j )
      {
        DomainType Hds;
        Hu[ j ].mv( ds, Hds );
        ret[ j ] -= ds * Hds;
      }
    }
#endif
    RangeType uVal;
    u( y, uVal );
    ret += uVal;
  }

  //! exact solution
  virtual void u(const DomainType& x,
                 RangeType& phi) const
  {
    assert( dimRange == 1 );
    phi[ 0 ] = 1;
    for( int i = 0; i < dimDomain; ++i )
      phi[ 0 ] *= std::cos( 2*M_PI*x[ i ] );
  }

  //! the jacobian of the exact solution
  virtual void uJacobian(const DomainType& x,
                         JacobianRangeType& ret) const
  {
    assert( dimRange == 1 );
    for( int i = 0; i < dimDomain; ++i )
    {
      ret[ 0 ][ i ] = -2*M_PI*std::sin( 2*M_PI*x[ i ] );
      for( int j = 1; j < dimDomain; ++j )
        ret[ 0 ][ i ] *= std::cos( 2*M_PI*x[ (i+j)%dimDomain ] );
    }
  }

  //! mass coefficient has to be 1 for this problem
  virtual void m(const DomainType& x, RangeType &m) const
  {
    m = RangeType(1);
  }

private:
  void hessian ( const DomainType &x, HessianRangeType &ret ) const
  {
    assert( dimRange == 1 );
    for( int i = 0; i < dimDomain; ++i )
    {
      for( int j = 0; j < dimDomain; ++j )
      {
        if ( j == i )
        {
          ret[ 0 ][ i ][ j ] = -4*M_PI*M_PI;
          for( int k = 0; k < dimDomain; ++k )
            ret[ 0 ][ i ][ j ] *= std::cos( 2*M_PI*x[ k ] );
        }
        else
        {
          ret[ 0 ][ i ][ j ] = 4*M_PI*M_PI;
          for( int k = 0; k < dimDomain; ++k )
            if ( k != i && k != j )
              ret[ 0 ][ i ][ j ] *= std::cos( 2*M_PI*x[ k ] );
            else
              ret[ 0 ][ i ][ j ] *= std::sin( 2*M_PI*x[ k ] );
        }
      }
    }
  }
};

#endif // #ifndef POISSON_HH
