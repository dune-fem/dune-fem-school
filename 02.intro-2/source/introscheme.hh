/*********************************************************/
/***                 NEW FOR LESSON 2                  ***/
/*********************************************************/
#ifndef INTRO_SCHEME_HH
#define INTRO_SCHEME_HH

// iostream includes
#include <iostream>

// include discrete function space
#include <dune/fem/space/lagrange.hh>

// include local interpolation
#include <dune/fem/space/common/interpolate.hh>

// adaptation ...
#include <dune/fem/function/adaptivefunction.hh>

// include linear operators
#include <dune/fem/operator/linear/spoperator.hh>
#include <dune/fem/solver/diagonalpreconditioner.hh>
#include <dune/fem/solver/cginverseoperator.hh>

/*********************************************************/

// include parameter handling
#include <dune/fem/io/parameter.hh>

#include "rhs.hh"

template < class Function >
class IntroScheme
{
public:
  typedef Function FunctionType ;

  //! grid view (e.g. leaf grid view) provided in the template argument list
  typedef typename FunctionType::GridPartType GridPartType;

  //! type of function space (scalar functions, \f$ f: \Omega -> R) \f$
  typedef typename FunctionType :: FunctionSpaceType   FunctionSpaceType;

  //! [Setting up the types for the discretization]
  //! choose type of discrete function space
  typedef Dune::Fem::LagrangeDiscreteFunctionSpace< FunctionSpaceType, GridPartType, POLORDER > DiscreteFunctionSpaceType;

  // choose type of discrete function
  typedef Dune::Fem::AdaptiveDiscreteFunction< DiscreteFunctionSpaceType > DiscreteFunctionType;
  //! [Setting up the types for the discretization]

  /*********************************************************/

  IntroScheme( GridPartType &gridPart, const FunctionType& function )
    : function_( function ),
      gridPart_( gridPart ),
      discreteSpace_( gridPart_ ),
      rhs_( "rhs functional", discreteSpace_ ),
      solution_( "solution", discreteSpace_ )
  {
    // set all DoF to zero
    solution_.clear();
  }

  const DiscreteFunctionType &solution() const
  {
    return solution_;
  }

  //! setup the right hand side
  void prepare()
  {
    assembleRHS ( function_, rhs_ );
  }

  //! solve the system
  void solve()
  {
  /*************************************************************************
   *** TODO: replace lagrange interpolation by inversion of mass matrix  ***
   *************************************************************************/
    Dune::Fem::interpolate( function_, solution_ );
  }

protected:
  const FunctionType& function_;

  GridPartType  &gridPart_;         // grid part(view), e.g. here the leaf grid the discrete space is build with

  DiscreteFunctionSpaceType discreteSpace_; // discrete function space
  DiscreteFunctionType rhs_;        // the rhs functional
  DiscreteFunctionType solution_;   // the unknown
};

#endif // end #if INTRO_SCHEME_HH
/*********************************************************/

