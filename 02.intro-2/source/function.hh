#ifndef DUNE_FEM_SCHOOL_INTRO_FUNCTIONS_HH
#define DUNE_FEM_SCHOOL_INTRO_FUNCTIONS_HH

// dune-common includes
#include <dune/common/documentation.hh>

  // QuadraticFunction
  // -----------------

  /*
   * A simple example function
   */
  template< class FunctionSpace >
  class QuadraticFunction
  {
    typedef QuadraticFunction< FunctionSpace > ThisType;

    static const int dimDomain = FunctionSpace::dimDomain;
    static const int dimRange = FunctionSpace::dimRange;

  public:
    typedef FunctionSpace FunctionSpaceType;
    typedef typename FunctionSpaceType::DomainType DomainType;
    typedef typename FunctionSpaceType::RangeType RangeType;

/*********************************************************/
/***                 NEW FOR LESSON 2                  ***/
/*********************************************************/
    typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;
/*********************************************************/

    void evaluate ( const DomainType &x, RangeType &value ) const
    {
      value = RangeType( 0 );

      value[ 0 ] = 1;
      for( int i = 0; i < dimDomain; ++i )
        value[ 0 ] *= x[ i ]*x[ i ];
    }

/*********************************************************/
/***                 NEW FOR LESSON 2                  ***/
/*********************************************************/
    void jacobian ( const DomainType &x, JacobianRangeType &jacobian ) const
    {
      jacobian = JacobianRangeType( 0 );

      for( int i = 0; i < dimDomain; ++i )
      {
        jacobian[ 0 ][ i ] = 2 * x[ i ];
        for( int j = 0; j < dimDomain; ++j )
          jacobian[ 0 ][ i ] *= (i == j ? 1.0 : x[ j ]*x[ j ]);
      }
    }
/*********************************************************/

  };

#endif // #ifndef DUNE_FEM_SCHOOL_INTRO_FUNCTIONS_HH
