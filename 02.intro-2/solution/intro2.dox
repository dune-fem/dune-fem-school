/** \page INTRO Getting started

    \subpage INTRO2 
    \page INTRO2 A L2-Projection

    In this example we show how to solve the following problem
    \f$ P[u] = f\f$ where \f$ P \f$ is the \f$ L^2 \f$-projection into the
    discrete space \f$ V_h \f$ satisfying for all \f$ \varphi\in V_h \f$:
    \f[ \int_\Omega P[u]\varphi = \int_\Omega f\varphi~. \f]
    Thus we need to assemble a matrix \f$ a_{ij}=\int_\Omega \varphi_j\varphi_i \f$
    and the right hand side functional \f$ l_i=\int_\Omega f\varphi_i \f$ for a
    basis function set \f$ (\varphi_i)_i \f$ of \f$ V_h \f$.

    This is done in the header file \link 02.intro-2/solution/rhs.hh rhs.hh\endlink within the two functions

    \snippet 02.intro-2/solution/rhs.hh Assemble functional and matrix

    \tableofcontents

    \section INTRO2RHS Right hand side assembly

    Let us start with the assembly of the right hand side functional
    \f{eqnarray*}{ 
    l(v) &:=& \int_\Omega f(x)v(x)\;dx \\ 
    &=& \sum_{T\in\Omega} \int_T f(x)v(x)\;dx~. \f}

    To calculate the integral we need a quadrature with weights \f$(w_\alpha)_{\alpha=1}^q\f$ 
    and quadrature points \f$(x_\alpha)_{\alpha=1}^q\f$ to approximate the integral.
    This leads to 
    \f[ \int_T f_T(x)\varphi^T_i(x)\;dx \approx \sum_{\alpha=1}^q w_\alpha f_T(x_\alpha)\varphi_i^T(x_\alpha) \f]
    where \f$ (\varphi^T_i)_i \f$ are the basis functions with support on \f$ T \f$.

    In dune-fem quadratures are defined on a reference element \f$R\f$ in local coordinates with weights 
    \f$(\hat{w}_\alpha)_{\alpha=1}^q\f$ and quadrature points \f$(\hat{x}_\alpha)_{\alpha=1}^q\f$.
    Let further \f$F_T:R \rightarrow T\f$ be a reference mapping and \f$ J(x)\f$ the jacobian of the reference mapping.
    Thus our quadrature rule can be described by a local quadrature rule.
    \f[ w_\alpha f_T(x_\alpha)\varphi_i^T(x_\alpha) = \mu(\hat{x}_\alpha)
    \hat{w}_\alpha f_T(F_T(\hat{x}_\alpha))\varphi_i^T(F_T(\hat{x}_\alpha))\f]  
    where we define the integration element by \f$\mu(x)=\sqrt{|\mathrm{det}(J^\top(x)J(x))|}\f$.

    Now we can calculate the approximation of the integral
    \f[ \sum_{T\in\Omega} \sum_{\alpha=1}^q\mu(\hat{x}_\alpha)
    \hat{w}_\alpha f_T(F_T(\hat{x}_\alpha))\varphi_i^T(F_T(\hat{x}_\alpha))\f]  
    by iterating over the elements of the grid using
    \func{begin()}/\func{end()} methods on the space which we obtain from the \co{rhs}
    discrete function.

    \snippet 02.intro-2/solution/rhs.hh Iterating over the space

    On each element \f$ T\in\Omega \f$ we first obtain the local representation of the function
    \f$ f_T=f|_T \f$ and the
    storage for the local contributions to the functional \f$ r=(r^T_i)_i \f$.

    \snippet 02.intro-2/solution/rhs.hh Get local representation of the function

    \note We assume that \f$ f \f$ has a local representation (i.e.
    \func{localFunction(entity)} can be called). If \f$ f \f$ is given analytically we need to
    again use the \class{GridFunctionAdapter} as described
    in the \ref INTRO1 "previous example".

    On the element we now need to compute
    \f[ r^T_i = r^T_i + \sum_{\alpha=1}^q\mu(\hat{x}_\alpha)
    \hat{w}_\alpha f_T(F_T(\hat{x}_\alpha))\varphi_i^T(F_T(\hat{x}_\alpha)). \f]

    The \class{CachingQuadrature} class is
    using caching of all basis functions values on the quadrature points in the reference element
    so that these are only computed once during a simulation. We can now iterate over all
    quadrature points

    \snippet 02.intro-2/solution/rhs.hh Apply quadrature rule

    Quadratures have a \func{point()} (\f$\hat{x}_\alpha\f$) and a \func{weight()} method (\f$\hat{w}_\alpha\f$). 
    One uses the square brackets operator \func{quadrature[p]} for
    evaluate methods so that the caching can be used (see further down for more details).

    The method \func{geometry.integrationElement( x )} returns the integration 
    element \f$\mu(x)\f$.

    \warning The \co{localFunction} and \co{rhsLocal} expect local coordinates. 
    No local to global mapping needs to be done. 
   
    \snippet 02.intro-2/solution/rhs.hh Computing the local contribution to the functional

    The final summation of the local contribution of that quadrature point is performed with the line
    \code
    rhsLocal.axpy( quadrature[ p ], f );
    \endcode
    \note Here, we are using caching again. 

    \section INTRO2MATRIX Matrix assembly

    The matrix assembly is very similar: The linear operator \f$ L \f$ maps
    from a discrete space \f$ V_h \f$ into the dual of a space \f$ W_h \f$. For a given discrete function
    \f$ u_h = \sum_{i=1}^N u_i \varphi_i \f$ we obtain a functional
    \f[ \langle L[u_h],v \rangle_{W_h^*,V_h} = \int_\Omega u_hv \f]
    Compare this with the right hand side functional we assembled previously
    \f[ \langle l,v \rangle_{W_h^*,V_h} = \int_\Omega fv \f]
    So assembly is therefore quite similar to what we described above.
    Of course we can use a bilinear form representation instead of the operator form:
    \f[ \langle L[u_h],v \rangle_{W_h^*,V_h} = a(u_h,v) \f]

    Before we start the assembly the linear operator need to be initialized

    \snippet 02.intro-2/solution/rhs.hh Setup linear operator

    First the sparsity pattern is past to the operator and then all entries are set to zero.
    The sparsity pattern contains all pairs of entities for which non zero entries in the
    matrix will have to be stored, i.e., all combinations of entities for which the
    method \func{localMatrix()} will be called (see below). In this case only pairs
    of the form \co{(entity,entity)} are needed for which a predefined stencil
    class exists within dune-fem.

    For the \co{rhs} we had a method to obtain the local contribution for a given entity
    \code
    rhs.localFunction(entity);
    \endcode
    The \class{LinearOperator} also has a localization method for the assembly
    taking two entities \f$ R,C \f$ and leading to the local contributions on
    \f$ R \f$ with the functions given by \f$ u_h=\varphi^C_k \f$. So we obtain the storage for
    entries of the form \f$ (\varphi^C_j\varphi^R_i)_{ij} \f$ using the same notation as before.
    In the case of the projection we are considering here only entries with \f$ R=C \f$ need to be
    considered because we need to compute
    \f[ \int_R \varphi^C_k\varphi^R_i \f]
    which is only non zero for \f$ R=C \f$.

    After obtaining the local contributions
    
    \snippet 02.intro-2/solution/rhs.hh Setup local Matrix and basis function set
    
    we again use a quadrature to approximate the integral
        
    \f[ \int_R \varphi_k^C \varphi_i^R \approx \sum_{\alpha=1}^q \mu(\hat{x}_\alpha)
    \hat{w}_\alpha \varphi_k^C(F_T(\hat{x}_\alpha)) \varphi_i^R(F_T(\hat{x}_\alpha))\f]

    The method \func{jLocal.column(k)}
    extracts the local contribution for the functionals with \f$ u_h = \varphi^C_k \f$ so the
    \func{axpy()} method on this object computes
    \f[ a^R_{ik} = a^R_{ik} + \varphi_k^T(x_\alpha)\varphi^T_i(x_\alpha) \f]

    The corresponding piece of code is

    \snippet 02.intro-2/solution/rhs.hh Computing the local contribution to the matrix

    \note Note the similarities to the assembly of the functional. Instead of evaluating a given
    function we need to evaluate all basis functions at a given point
    and then use an \func{axpy()} type method to add the contribution for that
    quadrature point to the matrix.

    \warning Again the \co{baseSet.evaluateAll()} method expects a local coordinate. 
    No local to global mapping needs to be done. 
   
    \section INTRO2SYSTEM Solving the assembled system

    To compute the projection given a function \f$ f \f$ (\link 02.intro-2/solution/function.hh function.hh\endlink)
    we need to invert the operator \f$ L \f$. There are a number of classes available to construct
    the inverse of a given linear operator. Here we use the class
    \class{CGInverseOperator}.
    All this is done within the file \link 02.intro-2/solution/introscheme.hh introscheme.hh\endlink
    in the method \func{IntroScheme::solve()}.

    \snippet 02.intro-2/solution/introscheme.hh Assemble the linear operator, construct the inverse operator, and solve the system

    This method in called in the \func{algorithm()} function after calling
    \func{IntroScheme::prepare()} where the right hand side assembly is carried out.
    All other central types for the discrete problem are set in the class \class{IntroScheme}
    as well - the discrete space,
    the type of the discrete function, and the type of the discrete linear operator. Note
    that there is some compatibility required especially between the type of the discrete function and
    the linear operator, e.g., if dune-istl is to be used then the correct types for both
    \class{DiscreteFunctionType} and \class{LinearOperatorType} have to be
    chosen.

    \snippet 02.intro-2/solution/introscheme.hh Setting up the types for the discretization

    The whole range of available solvers in dune-fem and the correct types to use can be
    found in the \ref SolverPage "solver example".

    \section INTRO2LOCALERROR Computing the local error

    The \func{algorithm()} method in \link 02.intro-2/solution/main.cc main.cc\endlink again start off by
    defining the function space and the grid in the same way discussed
    in the \ref INTRO1 "previous example".
    Then the scheme is constructed and 
    \code
    scheme.prepare();
    scheme.solve();
    \endcode
    is called to compute the \f$ L^2 \f$-projection. In addition to storing the exact
    solution and the discrete solution in a \co{vtk} file, using the
    \class{DataOutput} class, the error between the exact and discrete solution
    is also stored. Since only grid functions can be written, the error has to be
    converted using a class providing a \func{localFunction(entity)} method.
    Similar to the \class{GridFunctionAdapter} used to convert a global function
    into a grid function the \class{LocalFunctionAdapter} can be used to provide
    a grid function given an implementation of the local functions. The discretization error
    can be easily expressed in term of a local function class as shown by the
    \class{LocalError} class:

    \snippet 02.intro-2/solution/main.cc Implement a local function to use with a LocalFunctionAdapter

    The following shows how to use this class to construct a grid function

    \snippet 02.intro-2/solution/main.cc Use a local function Adapter to output the error on the grid

    Finally \link 02.intro-2/solution/main.cc main.cc\endlink contains a method to compute the \f$ L^2 \f$ error of
    the two grid functions. All concepts used here have been explained previously. The
    dune-fem package internally provides classes to compute different errors so that
    the computation of the error shown here could be replaced with something like the
    following
    \code
      Dune::Fem::L2Norm< GridPartType > norm( gridPart );
      return norm.distance( gridExactSolution, scheme.solution() );
    \endcode
**/
