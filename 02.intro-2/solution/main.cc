// always include this file
#include <config.h>

// C++ includes
#include <cmath>
#include <iostream>
#include <string>

// dune-common includes
#include <dune/common/exceptions.hh>                // adds Dune::Exception

// dune-fem includes
#include <dune/fem/gridpart/filteredgridpart.hh>    // adds Dune::Fem::FilteredGridPart
#include <dune/fem/gridpart/leafgridpart.hh>        // adds Dune::Fem::LeafGridPart
#include <dune/fem/io/file/dataoutput.hh>           // adds Dune::Fem::DataOutput
#include <dune/fem/io/parameter.hh>                 // adds Dune::Fem::Parameter
#include <dune/fem/misc/mpimanager.hh>              // adds Dune::Fem::MPIManager
#include <dune/fem/quadrature/cachingquadrature.hh> // adds Dune::Fem::CachingQuadrature
#include <dune/fem/space/common/adaptmanager.hh>    // adds Dune::Fem::GlobalRefine

/*********************************************************/
/***                 NEW FOR LESSON 2                  ***/
/*********************************************************/
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>
/*********************************************************/

#include <dune/fem/io/file/dataoutput.hh>

// local includes
#include "function.hh"                              // adds QuadraticFunction

/*********************************************************/
/***                 NEW FOR LESSON 2                  ***/
/*********************************************************/
#include "introscheme.hh"
/*********************************************************/

#include "radialfilter.hh"                          // adds RadialFilter

// LocalError
// ----------

//! [Implement a local function to use with a LocalFunctionAdapter]
template< class F1,class F2 >
struct LocalError
{
  // extract type of discrete function space
  typedef typename F1::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;

  // extract type of function space
  typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;

  // extract type of grid part
  typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;

  // extract type of element (entity of codimension 0)
  typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;

  // type of range values
  typedef typename FunctionSpaceType::RangeType RangeType;

  // constructor
  LocalError ( const F1 &f1, const F2 &f2 )
  : lf1_( f1 ),
    lf2_( f2 )
  {}

  // evaluate local function
  template< class PointType >
  void evaluate ( const PointType &x, RangeType &val )
  {
    RangeType tmp;
    lf1_.evaluate( x, val );
    lf2_.evaluate( x, tmp );
    val -= tmp;
  }

  // initialize to new entity
  void init ( const EntityType &entity )
  {
    lf1_.init( entity );
    lf2_.init( entity );
  }

private:
  typename F1::LocalFunctionType lf1_;
  typename F2::LocalFunctionType lf2_;
};
//! [Implement a local function to use with a LocalFunctionAdapter]

// MyDataOutputParameters
// ----------------------

struct MyDataOutputParameters
: public Dune::Fem::LocalParameter< Dune::Fem::DataOutputParameters, MyDataOutputParameters >
{
  MyDataOutputParameters ( const int step )
  : step_( step )
  {}

  MyDataOutputParameters ( const MyDataOutputParameters &other )
  : step_( other.step_ )
  {}

  virtual int startcounter () const
  {
    return step_;
  }

private:
  int step_;
};

/*********************************************************/
/***                 NEW FOR LESSON 2                  ***/
/*********************************************************/
template< class GridFunctionU, class GridFunctionV >
double computeError( const GridFunctionU &u, const GridFunctionV &v )
{
  // extract grid part
  // (and silently assume this also holds for GridFunctionV)
  typedef typename GridFunctionU::GridPartType GridPartType;
  const GridPartType &gridPart = u.gridPart();

  // initialize error (squared) to zero
  double error = 0;

  // obtian types of local functions
  typedef typename GridFunctionU::LocalFunctionType LocalFunctionUType;
  typedef typename GridFunctionV::LocalFunctionType LocalFunctionVType;

  // obtain type of iterator, entity and geometry
  typedef typename GridPartType::template Codim< 0 >::IteratorType IteratorType;
  typedef typename GridPartType::template Codim< 0 >::EntityType EntityType;
  typedef typename GridPartType::template Codim< 0 >::GeometryType GeometryType;

  // define type of quadrature to be used
  typedef Dune::Fem::CachingQuadrature< GridPartType, 0 > QuadratureType;

  // loop over the grid
  const IteratorType end = gridPart.template end< 0 >();
  for( IteratorType it = gridPart.template begin< 0 >(); it != end; ++it )
  {
    // obtain a reference to the entity
    const EntityType &entity = *it;

    // obtain geometry
    const GeometryType geometry = entity.geometry();

    // get the local representation of both functions on this entity...
    const LocalFunctionUType uLocal = u.localFunction( entity );
    const LocalFunctionVType vLocal = v.localFunction( entity );

    // get the correct quadrature order
    const int order = uLocal.order() + vLocal.order();
    // setup quadrature and loop over points
    QuadratureType quadrature( entity, order );
    for( unsigned int np = 0; np < quadrature.nop(); ++np )
    {
      // evaluate both grid functions
      typename LocalFunctionUType::RangeType uValue;
      uLocal.evaluate( quadrature[ np ], uValue );

      typename LocalFunctionVType::RangeType vValue;
      vLocal.evaluate( quadrature[ np ], vValue );

      // compute the norm square of the difference between both values
      const double diff = (uValue - vValue).two_norm2();

      // multiply by quadrature weight and integration element |DF_T| and sum up
      const double integrationElement = geometry.integrationElement( quadrature.point( np ) );
      error += diff * quadrature.weight( np ) * integrationElement;
    }
  }

  // finally take the square root
  return std::sqrt( error );
}
/*********************************************************/

// compute the size of the domain and the average of the exact solution
template< class HGridType >
double algorithm ( HGridType &grid, int step )
{
  // return value is some meassure for the error
  double error = 0;

  //! [Setup scalar function space]
  // use a scalar function space
  typedef Dune::Fem::FunctionSpace< double, double, HGridType::dimensionworld, 1 > FunctionSpaceType;

  // construct an instance of the problem
  typedef QuadraticFunction< FunctionSpaceType > FunctionType;
  FunctionType function;
  //! [Setup scalar function space]

  //! [Setup grid part to work on]
  // create host grid part consisting of leaf level elements
  typedef Dune::Fem::LeafGridPart< HGridType > HostGridPartType;
  HostGridPartType hostGridPart( grid );

  // create filter
  typedef RadialFilter< HostGridPartType > FilterType;
  typename FilterType::GlobalCoordinateType center( 0.5 );
  typename FilterType::ctype radius( .25 );
  FilterType filter( hostGridPart, center, radius );

  // create filtered grid part, only a subset of the host grid part's entities are contained
  typedef Dune::Fem::FilteredGridPart< HostGridPartType, FilterType > GridPartType;
  GridPartType gridPart( hostGridPart, filter );
  //! [Setup grid part to work on]

/*********************************************************/
/***                 NEW FOR LESSON 2                  ***/
/*********************************************************/
/*********************************************************/

  // convert the continuously given problem into a grid function
  typedef Dune::Fem::GridFunctionAdapter< FunctionType, GridPartType > GridExactSolutionType;
  GridExactSolutionType exact( "exact solution", function, gridPart, 5 );

/*********************************************************/
/***                 NEW FOR LESSON 2                  ***/
/*********************************************************/
  typedef IntroScheme< GridExactSolutionType > SchemeType;
  SchemeType scheme( gridPart, exact );
  scheme.prepare();
  scheme.solve();
  typedef typename SchemeType::DiscreteFunctionType DiscreteFunctionType;
  const DiscreteFunctionType &solution = scheme.solution();

  //! [Use a local function Adapter to output the error on the grid]
  // use a local function adapter to compute u - uh (result is a grid function)
  typedef LocalError< DiscreteFunctionType, GridExactSolutionType > LocalErrorType;
  typedef Dune::Fem::LocalFunctionAdapter< LocalErrorType > ErrorFunctionType;
  LocalErrorType localError( solution, exact );
  ErrorFunctionType errorFunction( "error", localError, gridPart );

  // setup tuple of functions to output (discrete solution, exact solution, and difference)
  typedef Dune::tuple< const DiscreteFunctionType *, GridExactSolutionType *, ErrorFunctionType * > IOTupleType;
  IOTupleType ioTuple( &solution, &exact, &errorFunction );
  //! [Use a local function Adapter to output the error on the grid]

/*********************************************************/

  // write data
  typedef Dune::Fem::DataOutput< HGridType, IOTupleType > DataOutputType;
  DataOutputType dataOutput( grid, ioTuple, MyDataOutputParameters( step ) );
  dataOutput.write();
  //! [Output the result for viewing]

/*********************************************************/
/***                 NEW FOR LESSON 2                  ***/
/*********************************************************/
  // compute the error
  error = computeError( solution, exact );
  return error;
/*********************************************************/

}

// -------------
// Main function
// -------------

int main ( int argc, char **argv )
try
{
  //! [Initialization of MPI, Parameter, Grid]
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );
  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );
  // append default parameter file
  Dune::Fem::Parameter::append( "../data/parameter" );
  // type of hierarchical grid
  typedef Dune::GridSelector::GridType HGridType;
  //! [Initialization of MPI, Parameter, Grid]

  //! [Create macro grid]
  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey( HGridType::dimension );
  const std::string gridfile = Dune::Fem::Parameter::getValue< std::string >( gridkey );

  // the method rank and size from MPIManager are static
  if( Dune::Fem::MPIManager::rank() == 0 )
    std::cout << "Loading macro grid: " << gridfile << std::endl;

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr( gridfile );
  HGridType &grid = *gridPtr;

  // do initial load balance
  grid.loadBalance();
  //! [Create macro grid]

  //! [Reading parameters]
  // read parameter: initial grid refinement
  const int level = Dune::Fem::Parameter::getValue< int >( "intro.level" );
  // read parameter: number of EOC loops
  const int repeats = Dune::Fem::Parameter::getValue< int >( "intro.repeats", 0 );
  // number of global refinements to bisect grid width
  const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();
  // globally refine grid such that the grid width is halfed level times
  Dune::Fem::GlobalRefine::apply( grid, level * refineStepsForHalf );
  //! [Reading parameters]

  //! [Calculating solution and error]
  // calculate first step
  double oldError = algorithm( grid, 0 );

  for( int step = 1; step <= repeats; ++step )
  {
    // refine grid globally (such that grid width is halfed)
    // note: This function correctly adjusts memory for discrete functions, etc.
    Dune::Fem::GlobalRefine::apply( grid, refineStepsForHalf );

    const double newError = algorithm( grid, step );
    const double eoc = log( oldError / newError ) / M_LN2;
    if( Dune::Fem::MPIManager::rank() == 0 )
    {
      std::cout << "Error: " << newError << std::endl;
      std::cout << "EOC( " << step << " ) = " << eoc << std::endl;
    }
    oldError = newError;
  }
  //! [Calculating solution and error]

  return 0;
}
catch( const Dune::Exception &exception )
{
  // handle Dune exceptions by simply displaying their message
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}

