/*********************************************************/
/***                 NEW FOR LESSON 2                  ***/
/*********************************************************/
#ifndef RHS_HH
#define RHS_HH

#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/operator/common/stencil.hh>

// assembleRHS
// -----------

//! [Assemble functional and matrix]
template< class Function, class DiscreteFunction >
void assembleRHS ( const Function &function, DiscreteFunction &rhs );
template< class LinearOperator >
void assembleMass ( LinearOperator &mass );
//! [Assemble functional and matrix]

template< class Function, class DiscreteFunction >
void assembleRHS ( const Function &function, DiscreteFunction &rhs )
{
  typedef typename DiscreteFunction::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunction::LocalFunctionType LocalFunctionType;

  typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
  typedef typename IteratorType::Entity EntityType;
  typedef typename EntityType::Geometry GeometryType;

  typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
  typedef Dune::Fem::CachingQuadrature< GridPartType, 0 > QuadratureType;

  //! [Iterating over the space]
  const DiscreteFunctionSpaceType &dfSpace = rhs.space();
  rhs.clear();

  const IteratorType end = dfSpace.end();
  for( IteratorType it = dfSpace.begin(); it != end; ++it )
  {
    //! [Iterating over the space]
    //! [Get local representation of the function]
    const EntityType &entity = *it;
    const GeometryType &geometry = entity.geometry();

    typename Function::LocalFunctionType localFunction =
             function.localFunction( entity);
    LocalFunctionType rhsLocal = rhs.localFunction( entity );
    //! [Get local representation of the function]

    //! [Apply quadrature rule]
    QuadratureType quadrature( entity, 2*dfSpace.order()+1 );
    const size_t numQuadraturePoints = quadrature.nop();
    for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
    {
      // obtain quadrature point
      const typename QuadratureType::CoordinateType &x = quadrature.point( pt );
      //! [Apply quadrature rule]

      //! [Computing the local contribution to the functional]
      // evaluate f
      typename Function::RangeType f;
      localFunction.evaluate( quadrature[ pt ], f );

      // multiply by quadrature weight
      f *= quadrature.weight( pt ) * geometry.integrationElement( x );

      // add f * phi_i to rhsLocal[ i ]
      rhsLocal.axpy( quadrature[ pt ], f );
      //! [Computing the local contribution to the functional]
    }
  }
  // exchange computed values during parallel computations
  rhs.communicate();
}

template< class LinearOperator >
void assembleMass ( LinearOperator &mass )
{
  typedef typename LinearOperator::LocalMatrixType LocalMatrixType;
  typedef typename LinearOperator::DomainFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType::BasisFunctionSetType BasisFunctionSetType;
  typedef typename DiscreteFunctionSpaceType :: RangeType RangeType;
  // typedef typename DiscreteFunctionSpaceType :: JacobianRangeType JacobianRangeType;
  typedef typename DiscreteFunctionSpaceType :: GridPartType GridPartType;
  typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
  typedef typename IteratorType::Entity       EntityType;
  typedef typename EntityType::Geometry       ElementGeometryType;
  typedef Dune::Fem::CachingQuadrature< GridPartType, 0 > QuadratureType;

  const DiscreteFunctionSpaceType &dfSpace = mass.domainSpace();

  //! [Setup linear operator]
  mass.reserve( Dune::Fem::DiagonalStencil<DiscreteFunctionSpaceType,DiscreteFunctionSpaceType>(dfSpace,dfSpace) );
  mass.clear();
  //! [Setup linear operator]

  const int blockSize = dfSpace.localBlockSize; // is equal to 1 for scalar functions
  std::vector< RangeType > phi( dfSpace.blockMapper().maxNumDofs()*blockSize );

  const IteratorType end = dfSpace.end();
  for( IteratorType it = dfSpace.begin(); it != end; ++it )
  {
    const EntityType &entity = *it;
    const ElementGeometryType &geometry = entity.geometry();

    //! [Setup local Matrix and basis function set]
    LocalMatrixType jLocal = mass.localMatrix( entity, entity );

    const BasisFunctionSetType &baseSet = jLocal.domainBasisFunctionSet();
    //! [Setup local Matrix and basis function set]
    const unsigned int numBasisFunctions = baseSet.size();

    QuadratureType quadrature( entity, 2*dfSpace.order() );
    const size_t numQuadraturePoints = quadrature.nop();
    for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
    {
      const typename QuadratureType::CoordinateType &x = quadrature.point( pt );
      const double weight = quadrature.weight( pt ) * geometry.integrationElement( x );

      //! [Computing the local contribution to the matrix]
      // evaluate all basis functions at given quadrature point
      baseSet.evaluateAll( quadrature[ pt ], phi );

      for( unsigned int localCol = 0; localCol < numBasisFunctions; ++localCol )
        jLocal.column( localCol ).axpy( phi, phi[localCol], weight );
      //! [Computing the local contribution to the matrix]
    }
  } // end grid traversal
}

#endif // #ifndef RHS_HH
/*********************************************************/

